angular.module('ui.quib.payments', [])

.directive('creditCard', [ '$log', 'UtilService', function ($log, UtilService) {
    return {
        restrict: 'EA',        
        scope: {  
        	bankingModel : '=',
        	ngShow : '='
        },        
        template:
        	'<div class="form-header-inner">Credit Card Details (AMEX, VISA and Mastercard Accepted)</div>' +
        	'<div class="form-section">' +           	
        	'<div class="form-input row" ><label required="true">Account Name</label><input id="ccName" name="ccName" type="text" ng-model="bankingModel.bankAccountName" ng-required="ngShow" class="longer" /></div>' +
    		'<div class="form-input row" ><label required="true">Credit Card Number</label><input id="cc" name="cc" type="text" ng-model="bankingModel.creditCardNumber" ui-mask="9999 9999 9999 999?9" ui-mask-placeholder ui-mask-placeholder-char="_" ng-required="ngShow" class="longer" /></div>' +
    		'<div class="form-input row" ><label required="true">Expiry Month</label><input id="mm" name="mm" type="text" ng-model="bankingModel.creditCardExpiry" ui-mask="9999" ui-mask-placeholder="MMYY" ng-required="ngShow" class="longer" /></div>'+    		    		
    		'<div class="form-input row" ><label required="true">CVV</label><input type="text" id="cvv" name="cvv" ng-model="bankingModel.creditCardCVV" ui-mask="{{cvvMask}}" ui-mask-placeholder-char="_" ng-required="ngShow" /></div>' +
    		'<div class="form-input row" >Please note a surcharge of 1.25% for VISA and MasterCard, and 2.95% for AMEX cards will be added to the transaction.</div>' +
    		'</div>'
        ,
        link: function(scope, iElement, iAttrs) {  
        	 if( !scope.ngShow ) {
        		 scope.ngShow = true;
        	 }  
        	 
        	 scope.cvvMask = "";
        	 scope.$watch( 'bankingModel.creditCardNumber' , function() {
        		 var $type = UtilService.getCreditCardType( scope.bankingModel.creditCardNumber );
        		 $log.info('Card Type : ' + $type);
        		 if( $type == "AMEX" ) {
        			 scope.cvvMask = "9999";
        		 }
        		 else {
        			 scope.cvvMask = "999";
        		 }
        		 
        	 } );        	 
        	 
       }
    };
}])
.directive('directDebit', ['$log', function ( $log) {
    return {
        restrict: 'EA',        
        scope: {  
        	bankingModel : '=',
        	ngShow : '='
        },        
        template:
        	'<div class="form-header-inner">Debit Order Details</div>' +
        	'<div class="form-section">' +   
    		'<div class="form-input row" ><label required="true">Account Name</label><input id="ddName" name="ddName" type="text" ng-model="bankingModel.bankAccountName" ng-required="ngShow" class="longer" /></div>' +
    		'<div class="form-input row" ><label required="true">BSB</label><input id="bsb" name="bsb" type="text" ng-model="bankingModel.bankBranch" ui-mask="999-999" ui-mask-placeholder ui-mask-placeholder-char="_" ng-required="ngShow" /></div>' +
    		'<div class="form-input row" ><label required="true">Account Number</label><input id="ddacc" name="ddacc" type="text" ng-model="bankingModel.bankAccountNumber" ng-required="ngShow" class="longer" /></div>' +
    		'</div>'
        ,
        link: function(scope, iElement, iAttrs) {  
     	             	
       }
    };
}])
.directive('eventFocus', function(focus) {
    return function(scope, elem, attr) {
      elem.on(attr.eventFocus, function() {
        focus(attr.eventFocusId);
      });

      // Removes bound events in the element itself
      // when the scope is destroyed
      scope.$on('$destroy', function() {
        elem.off(attr.eventFocus);
      });
    };
  })
  .directive('googleplace', function() {
    return {
        restrict: 'EA',              
        require: 'ngModel',
        scope: {
            ngModel: '=',
            details: '='
        },        
        link: function(scope, element, attrs, model) {
            var options = {
                types: [],
                componentRestrictions: {country: 'au'}
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'short_name',
                country: 'long_name',
                postal_code: 'short_name'
            };
            
            var nameMap = {
                street_number: 'streetNumber',
                route : 'streetAddress',
                locality : 'suburb',
                administrative_area_level_1 : 'state',
                postal_code : 'postalCode'                
            };


            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {              
              //scope.details = scope.gPlace.getPlace();
              var place = scope.gPlace.getPlace();
              model.$setViewValue(element.val());         
              
              for (var i = 0; i < place.address_components.length; i++) {
                  var addressType = place.address_components[i].types[0];        
                  //if (componentForm[addressType]) {            
                      var val = place.address_components[i][componentForm[addressType]];
                      var name = nameMap[addressType];
                      if( val && name ) {                        
                        scope.details[name] = val;
                        console.info(name + ' ' + val);
                      }
                  //}
              }              
                     
              /*
              scope.$apply(function() {
                  scope.details = scope.gPlace.getPlace();
                  
              });*/
            });
        }
    };
});