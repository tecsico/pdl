(function () {
    'use strict';

    angular
        .module('app')
        .controller('newleadController', NewleadController );

    NewleadController.$inject = ['$scope', '$http', '$log', '$routeParams', '$location', '$rootScope', 'QuoteService', 'AuthService','focus', '$document','$filter', 'QuibQuoteService'];

    
	// create the controller and inject Angular's $scope

    function NewleadController($scope, $http, $log, $routeParams, $location, $rootScope, QuoteService, AuthService, focus, $document, $filter, QuibQuoteService ) {
    	
      function formatDate($date) {
          return $filter('date')($date, 'yyyy-MM-dd');
      }
      
      function monthDiff(d1, d2) {
          var months;                    
          months = (d2.getFullYear() - d1.getFullYear()) * 12;          
          months -= d1.getMonth() + 1;          
          months += d2.getMonth();
          
          if( months == 0 )
            return 12;
          if( months < 0 ) 
            return 12 + months;
          else
            return months <= 0 ? 0 : months;
      }
      
      function getDate( $dateString ) {
         var $splitValues = $dateString.split("-");
         return new Date($splitValues[0], $splitValues[1] - 1, $splitValues[2]);
      }
      
      function roundTo(n, digits) {
          var negative = false;
          if (digits === undefined) {
              digits = 0;
          }
              if( n < 0) {
              negative = true;
            n = n * -1;
          }
          var multiplicator = Math.pow(10, digits);
          n = parseFloat((n * multiplicator).toFixed(11));
          n = (Math.round(n) / multiplicator).toFixed(2);
          if( negative ) {    
              n = (n * -1).toFixed(2);
          }
          return n;
      }
      
      function getPrice( $state, $months ) {
          $scope.pricing.forEach(
              function($item) {                
                 if($item.state == $state) {
                   $item.prices.forEach(
                     function($price){                       
                       if( $price.months == $months ) {
                         $log.info('item ' + angular.toJson($price));
                         //return $price;
                         $scope.quote.premium = {                           
                           "basePremium" : $price.price,
                           "totalAnualPremium" : $price.price,
                           "totalMonthPremium" : roundTo($price.price/$months, 2)
                       };
                     }
                   }
                   );
                 }
              }            
          );          
      }

      function getProperties( ) {	        
          $http.get('properties/pricing.json').success(function (data) {              
              $scope.pricing = data;	          
          });		             
      }
      
      getProperties();
      
      $scope.roles = ['Community Pharmacist', 'Pharmacy Business Owner','Hospital Pharmacist','Clinical Pharmacist','Research Pharmacist','Locum','Intern','Student','Other Pharmacist'];      
      $scope.titles = ['Mr','Miss','Prof','Dr','Mrs'];
      $scope.paymentDetails = {};
      /*
      $scope.quote = {
        "startDate" : formatDate(new Date())
      };
      */
      $scope.dutyAccepted = false;
      $scope.quoteAccepted = false;
      
      $scope.setPriceOnQuote = function() {
        $scope.quotePriced = true;
        $scope.quote.endDate = new Date(2018, 4, 1);
        
        var m = monthDiff( getDate($scope.startDate) , $scope.quote.endDate );
        if( m == 12 ) {
          m = 12;
        }
        $scope.quote.months = m;
        getPrice('vic', m);  
      }
      
      $scope.priceQuote = function() {
        $scope.createQuote();  
      }
      
      $scope.defaultCreditCard = function() {
          $scope.paymentDetails.paymentType = 'credit-card';
      }
      
      /*
    	$scope.processingQuickQuote = false;
    	$scope.processingQuote = false;
    	$scope.states = [];   
    	$scope.paymentMethods = [];//$rootScope.properties.paymentMethods;
    	
    	$scope.quote = {
    			"paymentMethod":"CC"
    	};
    	$scope.quotepi = {"premium":0.00};
    	$scope.quoteml = {"premium":0.00};
      */
      
      $scope.lead = {
        "company": "aua",
        "title": {
          "id": "mr"
        },
        "name": "Ferdi",
        "surname": "Stark",
        "mobile": "0449141467",
        "email": "ferdi@tecsico.com.au",
        "dateOfBirth": "1980-07-15",
        "physicalAddress": {
          "streetAddress": "",
          "postalCode": "",
          "state": "",
          "suburb": "",
          "addressPostalCode": "",
          "country": "AUSTRALIA",
          "streetType": "",
          "streetNumber": "",
          "addressNotFound": false
        }
      };
    	
      $scope.quote = {
          "startDate" : formatDate(new Date()),
          "company": "aua",
          "lead": null,
          "channel": {
            "id": "compare-and-connect"
          },
          "product" : {
            "id" : null
          },
          "ownerType" : {
          	"id" : "owner"
          },
          "property" : {
          	"state" : "vic",
          	"postalCode" : "3136",
          	"suburb" : "Croydon",
          	"constructionType" : {
          		"id" : "brick-veneer"
          	}
          },
          "policyRules" : []
        };
      
    	$scope.client = {
    			  "name": "",
    			  "registeredName": "",
    			  "brokerId": null,
    			  "postalAddress": {
    			    "address1": "",
    			    "address2": null,
    			    "suburb": "",
    			    "state": "",
    			    "postCode": "",
    			    "country": "AUSTRALIA"
    			  },
    			  "streetAddress": {
    			    "address1": "",
    			    "address2": "",
    			    "suburb": "",
    			    "state": "",
    			    "postCode": "",
    			    "country": "AUSTRALIA"
    			  },
    			  "telephone": "",
    			  "fax": "",
    			  "email": "",
    			  "fsraType": null,
    			  "abn": "" ,
    			  "contact": {
    				    "name": "",
    				    "telephone": "",
    				    "mobile": "",
    				    "fax": "",
    				    "email": ""
    				  },    			  
    	};
    	
    	$scope.bankingModel = {    		
    	};    	
    	/*
    	$rootScope.$watch( 'properties', function(){
    		if( $rootScope.properties.states ) {
    			$scope.states = $rootScope.properties.states;
    		}
    		if( $rootScope.properties.paymentMethods ) {
    			$scope.paymentMethods = $rootScope.properties.paymentMethods;
    		}
        	$scope.policy = {
            		"clazz":"PI",
            		"underwriter": $rootScope.properties.policyDefaults.underwriter,
            		"policyNumber" : "",
            		"invoiceNumber" : "",
            		"inceptionDate": $rootScope.properties.policyDefaults.inceptionDate,
            		"expiryDate": $rootScope.properties.policyDefaults.expiryDate,
            		"insuredName": "",
            		"previouslyFunded": "N",
            		"coInsurance": "N",
            		"fsraType": "F",
            		"secondaryReference": "",
            		"fundLoanNumber": null,
            		"basePremium": 0.0,
            		"basePremiumGst": 0.0,
            		"stampDuty": 0.0,
            		"fsl": 0,
            		"brokerFee": 0.0,
            		"brokerFeeGst": 0.0,
            		"commission": 0,
            		"commissionGst": 0,
            		"premium": 0.0,
            		"numberOfPayments": 10    			
            	};
        	login();
        	//check if we need to preload the quote...
        	if( $routeParams.quoteId ) {        		
        		$log.info('Quote Id ' + $routeParams.quoteId );
        		QuoteService.getQuote( null, $routeParams.quoteId )
        		.then(  
        				function ( $response ) {
        					$log.info( 'Get Quote : ' + angular.toJson( $response ) );
        					var $client = $response.data.client;
        					var $policies = $response.data.policies;
        					
							$scope.client.name = $client.name;
							$scope.client.abn = $client.abn;    
							$scope.client.telephone = $client.telephone;
							$scope.client.email = $client.email;
							
							$scope.client.postalAddress.address1 = $client.addressPostLine1;
							$scope.client.postalAddress.address2 = $client.addressPostLine2;
							$scope.client.postalAddress.postCode = $client.addressPostCode;
							$scope.client.postalAddress.suburb = $client.addressPostSuburb;
							$scope.client.postalAddress.state = $client.addressPostState;
							
							$policies.forEach(
									function($item) {
										$log.info('Got Item : ' + angular.toJson($item))
										$scope.quotepi.policyNumber = $item.policyNumber;
										$scope.quotepi.invoiceNumber = $item.policyNumber;
										$scope.quotepi.premium = $item.premium;
									}
							);  
													
        				}
        		);        		
        	}        	
        	
    	} );
      */
      
      //MED0001177696
      
    /*  
    $scope.chosenPlaceDetails = {};
    
    $scope.$watch('chosenPlace', function(){
      $log.info(angular.toJson($scope.quote.property));
      $log.info(angular.toJson($scope.chosenPlace));
      $scope.quote.property.state = $scope.chosenPlaceDetails;
    });  
    */  
    function createQuibQuoteRequest(  ) {
      $scope.quote.lead = $scope.lead;      
      return {
        "quote" : $scope.quote
      };
    }  
      
    $scope.createQuote = function() {
      var $request = createQuibQuoteRequest();
      QuibQuoteService.updateQuotePolicyRule( $request.quote, 'duty-of-disclosure', 'true' );

      //$log.info('create quote request : ' + angular.toJson($request));
      $scope.startDate = formatDate($request.quote.startDate);
      $scope.roleType = $request.quote.roleType;
      QuibQuoteService.createQuote($request)
      .then(
        function($response) {
          $scope.quote = $response.data.quote;
          $scope.quote.startDate = $scope.startDate;
          //$log.info('create quote response : ' + angular.toJson($scope.quote));
          $scope.quote.neverDeclined = 'false';
          $scope.quote.roleType = $scope.roleType;
          
          $scope.setPriceOnQuote();
        }
      );
      /*
      .error(
        function($response) {
          $log.info(angular.toJson($response));
        }
      );
      */      
    } ;
    
    function mapPayment($request) {
      
      $request.payment.paymentFrequency = {
        "id" : $scope.paymentDetails.paymentFrequency
      };
      $request.payment.paymentType = {
        "id" : $scope.paymentDetails.paymentType
      };      
      $request.payment.accountType = "CHEQUE";      
      
      $request.payment.name = $scope.quote.accountName;
      
      if( $scope.paymentDetails.paymentType == 'credit-card') {
        $request.payment.creditCardNumber = $scope.paymentDetails.creditCardNumber;
        $request.payment.name = $scope.paymentDetails.creditCardName;
        $request.payment.cvv = $scope.paymentDetails.cvv;
        //"accountType": "VISA",
        $request.payment.expiryYear = $scope.paymentDetails.expiry;
        $request.payment.expiryMonth = $scope.paymentDetails.expiry;
      }
      else {
        $request.payment.accountNumber = $scope.paymentDetails.accountNumber;
        $request.payment.bsb = $scope.paymentDetails.accountBsb;
        $request.payment.name = $scope.paymentDetails.accountName;
      }
      
      return $request;

      /*
     {
        "company": $scope.policy.channel.id,        
        "paymentFrequency":{
           "id" : "Y"
        },
        "paymentType":{
          "id" : "credit-card"
         },
        "accountNumber": null,
        "accountType": "VISA",
        "bsb": null,
        "cvv":"847",
        "expiryYear":"19",
        "expiryMonth":"02",
        "name":"Mr S Gell",
        "policy": 818,
        "premium": $scope.quote.premium.totalAnualPremium,
        "gatewayReference": null,
        "completed": false,
        "policyNumber": "AUAP-2017-00000818",
        "createDate": 1510815420000,
        "receivedDate": null,
        "taxAmount": 15,
        "exclusiveAmount": 161,
        "status": "NEW",
        "creditCardNumber":"4564710000000004",
        "dueDate": null,
        "transactionType": "PREMIUM",
        "stampDuty": 11,
        "fsl": 0,
        "basePremium": $scope.quote.premium.basePremium,
        "policyFee": 50,
        "externalReference": null,
        "riskAddress": null,
        "reminderDate": null,
        "noticeDate": null,
        "noticeCount": 0,
        "lapsingDate": null
      }
      */
    }
    //$scope.acceptResponse = true;
    $scope.acceptQuote = function() {
        $scope.processingQuote = true;
          var $request = createQuibQuoteRequest();
          QuibQuoteService.updateQuotePolicyRule( $request.quote, 'duty-of-disclosure', 'true' );
          $request.quote.quoteRiskItems = [
            {
              "riskCode" : "PI",
              "riskPremium" : $request.quote.premium.totalAnualPremium,
              "finalPremium" : $request.quote.premium.basePremium
            }
          ];          
          
          $scope.startDate = formatDate($request.quote.startDate);
          $scope.roleType = $request.quote.roleType;      
          $scope.months = $scope.quote.months;      
          $request.quote.decision = {
              "id" : "approved"
          };
          
          //$log.info('update quote request : ' + angular.toJson($request));
                  
          QuibQuoteService.updateQuote($request)
          .then(
            function($response) {
              $scope.quote = $response.data.quote;
              //$log.info('update quote response : ' + angular.toJson($scope.quote));
              $scope.quote.neverDeclined = 'false';
              $scope.quote.roleType = $scope.roleType;
              $scope.quote.months = $scope.months;
              $request = {                
                  "quoteNumber" : $scope.quote.quoteNumber
              };
              QuibQuoteService.createPolicy( $request ) 
              .then (
                function($response) {
                  $scope.policy = $response.data.policy;
                  $scope.payment = $response.data.payment;
                  $scope.payment.policyNumber = $scope.policy.policyNumber;
                  
                  console.log('payment details : ' + angular.toJson($scope.payment));                  
                  $request = {
                    "policy" : {
                  	   "policyNumber": $scope.policy.policyNumber  
                    },
                    "payment" : $scope.payment
                  };
                  $request = mapPayment( $request );
                  $request.policy.paymentFrequency = $request.payment.paymentFrequency;
                  console.log('payment request : ' + angular.toJson($request) );
                  QuibQuoteService.debitPolicy( $request )
                  .then(
                    function($response) {
                      console.log( 'payment response : ' + angular.toJson($response) );
                      if( $response.data.success ) {
                        $scope.acceptResponse = true;
                      }
                      $scope.processingQuote = false;
                    }
                  ).catch(function($response){
                    $scope.processingQuote = false;
                  });
                }
              ).catch(function($response){
                $scope.processingQuote = false;
              });                         
            }
          ).catch(function($response){
            $scope.processingQuote = false;
          });
    };
    
    $scope.getPaymentFrequency = function($id) {
      return ($id == 'M') ? 'Monthly' : 'Annual';
    }  
      
      
		function login(){
			//$scope.message = 'Authenticating...';
			var $data = AuthService.authenticate( $rootScope.properties.credentials.username, $rootScope.properties.credentials.password );
			
			$data.success( function ($data) {
					$log.info($data);
					if( $data.result == 'Success' ) {						
						AuthService.setCredentials( $rootScope.properties.credentials.username ,$data.session );						
						//$scope.message = 'Logged on ' + $data.session;
					}
					else {
						//$scope.message = 'Could not log on: ' + $data.error.message;
					}
				}
			).error(
				function () {
					//$scope.message = 'Failed to call quote API.';
				}
			);						
		};    	
    	
    	
    	
    	$scope.submitLead = function() {
    		$scope.processingQuote = true;
    		//$scope.policy.numberOfPayments = $scope.quote.paymentOption.numberOfInstalments;
    		$scope.policy.premium = $scope.getTotalPremium();
    		$scope.policy.policyNumber = $scope.quotepi.policyNumber;
    		$scope.policy.invoiceNumber = $scope.quotepi.policyNumber;
    		
    		var $quoteRequest = $scope.createQuoteRequest();    		
    		
    		QuoteService.quote( AuthService.getCredentials().session, $quoteRequest )
    		.success( 
    				function(response) {
    					$log.info('Quote Response :' + angular.toJson(response));
    					$scope.quoteResponse = response;
    					
    					if( $scope.quoteResponse.result == 'Success' ) {
    						var $acceptRequest = $scope.createAcceptRequest($scope.quoteResponse.quoteNumber);
    						QuoteService.accept( AuthService.getCredentials().session, $acceptRequest )
    						.success( 
    								function(response) {    									    									
    									if( response.result == 'Success' ) {
    										//$location.path('/leadsubmitted');
    										$scope.acceptResponse = response;
    										$scope.processingQuote = false;
    										focus('submitted');
    									}
    									else {
    										//need to show the error message...
    										$scope.processingQuote = false;
    										$scope.message = response.error.message;
    									}
    								}
    						)
    						.error(
    								function(response) {
    									$scope.processingQuote = false;
    								}
    						);
    					}
    					else {
    						$scope.processingQuote = false;
    						$scope.message = response.error.message;
    					}
    					
    				}
    		)
    		.error( 
    				function(response) {
    					$scope.processingQuote = false;
    				}
    		);
    		
    		//$location.path('/leadsubmitted');
    	};  	
    	
    	$scope.createQuoteRequest = function() {
    		var $request = {
    				"code" : "NewQuote",
    				"session" : AuthService.getCredentials().session,
    				"client" : $scope.client,
    				"policies" : [$scope.policy]
    		};
    		return $request;
    	};
    	
    	$scope.createAcceptRequest = function($quoteId) {
    		var $request = {
    				"code" : "AcceptQuote",
    				"session" : AuthService.getCredentials().session,
    				"quoteNumber" :  $quoteId,
    				"bankingDetails": [
   				       
    				]
    		};
    		
    		if( $scope.quote.paymentMethod == 'CC' ) {
    			$request.bankingDetails.push(
        				{
	                	      "paymentOrder": 0,
	                	      "paymentType": $scope.quote.paymentMethod,
	                	      "bankBranch": "",
	                	      "bankAccountNumber": "",
	                	      "bankAccountName": $scope.bankingModel.bankAccountName,
	                	      "creditCardNumber": $scope.bankingModel.creditCardNumber,
	                	      "creditCardExpiry": $scope.bankingModel.creditCardExpiry,
	                	      "creditCardCVV": $scope.bankingModel.creditCardCVV
	                    } 
        		);
    		}
    		else {
    			$request.bankingDetails.push(
        				{
	                	      "paymentOrder": 0,
	                	      "paymentType": $scope.quote.paymentMethod,
	                	      "bankBranch": $scope.bankingModel.bankBranch,
	                	      "bankAccountNumber": $scope.bankingModel.bankAccountNumber,
	                	      "bankAccountName": $scope.bankingModel.bankAccountName,
	                	      "creditCardNumber": "",
	                	      "creditCardExpiry": "",
	                	      "creditCardCVV": ""
	                    } 
        		);    			
    		}
    		
    		
    		return $request;
    	};
    	
    	$scope.getQuote = function(instalment) {
    		if( $scope.quickQuotes ) {
	    		$scope.quickQuotes.forEach(
	    				function( $item ) {
	    					if( $item.numberOfInstalments == instalment ) {
	    						$log.info('Found ' + angular.toJson($item));
	    						return $item.instalmentAmount;
	    					}
	    				}
	    		);
    		}
    	};
    	
    	$scope.getTotalPremium = function() {
    		var $total = 0.0;
    		if( $scope.quotepi && $scope.quotepi.premium ) {
    			$total += $scope.quotepi.premium;
    		}
    		if( $scope.quoteml && $scope.quoteml.premium ) {
    			$total = $scope.quotepi.premium + $scope.quoteml.premium;
    		}
    		return Math.round($total * 100) / 100;
    	};
    	/*
    	$scope.getInterest = function() {
    		if( $scope.quote && $scope.quote.paymentOption ) {
    			return ( $scope.quote.paymentOption.instalmentAmount * $scope.quote.paymentOption.numberOfInstalments ) - $scope.getTotalPremium();
    		}
    		return 0.0;    		
    	}
    	  
    	$scope.getTotalRepayable = function() {
    		if( $scope.quote && $scope.quote.paymentOption ) {
    			return ( $scope.quote.paymentOption.instalmentAmount * $scope.quote.paymentOption.numberOfInstalments );
    		}
    		return 0.0;
    	}
    	*/
		$scope.calculate = function () {
			$scope.processingQuickQuote = true;
			var $totalPremium = $scope.getTotalPremium();
			var quickQuotes = [
			    {
					"key" : "Q08",
					"amount" : $totalPremium,
					"numberOfInstalments" : 8,
					"numberOfSettlementDays" : 27
				},
			    {
					"key" : "Q10",
					"amount" : $totalPremium,
					"numberOfInstalments" : 10,
					"numberOfSettlementDays" : 27
				},
			    {
					"key" : "Q12",
					"amount" : $totalPremium,
					"numberOfInstalments" : 12,
					"numberOfSettlementDays" : 27
				}				
			];
			
			var request = {
					"session" : AuthService.getCredentials().session,
					"quickQuotes" : quickQuotes					
			};
			$log.info( angular.toJson(request) );
			QuoteService.quickQuote( AuthService.getCredentials().session, request )
			.success(
					function(response) {
						$scope.quickQuotes = response.quickQuotes;
						$log.info( 'quick quotes : ' + angular.toJson( $scope.quickQuotes ) );
						$scope.processingQuickQuote = false;
						//$scope.quote.paymentOption = null;
					}
			);
		};
		
		$scope.quoted = function () {
			return ($scope.quickQuotes);
		};
		/*
		$scope.instalmentOptionSelected = function() {
			return ($scope.quote.paymentOption);
		};
		*/
		$scope.submitted = function() {
			return ($scope.acceptResponse);
		};
		
		$scope.quickQuoteValid = function() {
			var $total = $scope.getTotalPremium();
			return ( 0 < $total );			
		};
		
		$scope.$watch('quotepi.premium', function() {
			$scope.quote.paymentOption = null;
			$scope.quickQuotes = null;
		});

		$scope.$watch('quoteml.premium', function() {
			//$scope.quote.paymentOption = null;
			$scope.quickQuotes = null;
		});
		
	}

})();
