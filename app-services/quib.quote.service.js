(function () {
    'use strict';

    angular
        .module('app')
        .service( 'QuibQuoteService', QuibQuoteService );

    	QuibQuoteService.$inject = ['$http', '$log', '$rootScope', 'QuotePolicyRule'];
    
		function QuibQuoteService( $http, $log, $rootScope, QuotePolicyRule ) {				
		 
		  var service = {};
		  service.createQuote = createQuote;
      service.updateQuotePolicyRule = updateQuotePolicyRule;
      service.updateQuote = updateQuote;
      service.createPolicy = createPolicy;
      service.debitPolicy = debitPolicy;
      
		  service.createAccept = createAccept;
		  service.accept = accept;
		  service.acceptPayment = acceptPayment;
		  service.quickQuote = quickQuote;
		  service.getQuote = getQuote;		  
		  return service;
		  
      function updateQuotePolicyRule($quote, $field, $value) {
          if ($quote != undefined &&
                  $quote.policyRules != undefined &&
                  $field != undefined &&
                  $field != null) {
              //$log.info('updateQuotePolicyRule ' + $field);

              var found = false;
              $quote.policyRules.forEach(
                      function (item) {
                          if (item.ruleId == $field) {
                              //$log.info('Found field ' + $field + ' ' + $value);
                              item.value = $value;
                              found = true;
                          }
                      }
              );

              if (!found) {
                  //$log.info('Adding rule id : ' + $field + ' ' + $value);
                  //add the quote policy rule...
                  var rule = new QuotePolicyRule();
                  rule.ruleId = $field;
                  rule.value = $value;
                  $quote.policyRules.push(rule);
              }
          }
      }
      
      function createQuote($quote) {
        $http.defaults.headers.common['Authorization'] = '123123123';
        return $http.post( $rootScope.properties.quib + 'api/createQuote', $quote);
		  }      
      
      function updateQuote($quote) {
        $http.defaults.headers.common['Authorization'] = '123123123';
        return $http.post( $rootScope.properties.quib + 'api/updateQuote', $quote);
		  }       
      
      function createPolicy( $request ) {
        $http.defaults.headers.common['Authorization'] = '123123123';
        return $http.post( $rootScope.properties.quib + 'api/createPolicy', $request);
      }
      
      function debitPolicy( $request ) {
        $http.defaults.headers.common['Authorization'] = '123123123';
        return $http.post( $rootScope.properties.quib + 'api/debitPolicy', $request);
      }      
      
		  function accept( $sessionId, $quote ) {
			  $quote.session = $sessionId;
			  $log.debug($quote);
		      var promise = $http.post( $rootScope.properties.services + 'interface/accept/json', $quote).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }		  

		  function acceptPayment( $sessionId, $quote ) {
			  $quote.session = $sessionId;
			  $log.debug($quote);
		      var promise = $http.post( $rootScope.properties.services + 'interface/acceptPayment/json', $quote).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }	
		  

		  
		  function quickQuote( $sessionId, $quote ) {
			  $quote.session = $sessionId;
			  $log.debug($quote);
		      var promise = $http.post( $rootScope.properties.services + 'interface/quickquote/json', $quote).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }	
		  
		  function getQuote( $brokerId, $quoteId ) {
			  var $request = {
						"brokerId" : $brokerId,
					  	"externalReference" : $quoteId
					  };
			  $log.debug($request);
		      var promise = $http.post( $rootScope.properties.services + 'interface/retrieveQuote/json', $request).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }					  
		  
		  function createAccept($quoteNumber) {
			  $log.info('Creating accept request...');
			  return {
				"code" : "AcceptQuote",
				"quoteNumber" : $quoteNumber,
				"session" : "",
				"bankingDetails": 
					[
					]
			  };
		  }
		  
		}

})();