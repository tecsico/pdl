(function () {
    'use strict';

    angular
        .module('app')
        .service( 'AdminService', AdminService );

    	AdminService.$inject = ['$http', '$log', '$cookies', '$rootScope', 'AuthService'];
    
		function AdminService( $http, $log, $cookies, $rootScope, AuthService ) {				
		 
		  var service = {};
		  service.getBrokers = getBrokers;
		  service.getUsers = getUsers;
		  service.addUserBroker = addUserBroker;
		  service.removeUserBroker = removeUserBroker;
		  service.getQuotes = getQuotes;
		  service.getAuditEntries = getAuditEntries;
		  return service;
		  
		  function getBrokers( ) {				  
		      var promise = $http.get( $rootScope.properties.services + 'admin/listBrokers').success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }
		  
		  function getUsers( ) {				  
		      var promise = $http.get( $rootScope.properties.services + 'admin/listUsers').success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }
		  
		  function getQuotes( $broker, $date ) {	
		      var promise = $http.post( $rootScope.properties.services + 'admin/listQuotes/' , { 'brokerId': $broker, 'searchDate': $date }).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }		  
		  
		  function addUserBroker( $brokerId, $username ) {
			  var $userBroker = {
					  "brokerId" : $brokerId,
					  "username" : $username
			  };
		      var promise = $http.put( $rootScope.properties.services + 'admin/userBroker', $userBroker).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }
		  
		  function removeUserBroker( $userBroker ) {
		      var promise = $http.delete( $rootScope.properties.services + 'admin/userBroker/' + $userBroker.username ).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }
		  
		  function getAuditEntries( $quoteNumber ) {			  
		      var promise = $http.get( $rootScope.properties.services + 'admin/listAuditEntriesByQuote/' + $quoteNumber).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }
	
		}

})();