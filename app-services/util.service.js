(function () {
    'use strict';

    angular
        .module('app')
        .service( 'UtilService', UtilService );

    	UtilService.$inject = ['$log'];
    
		function UtilService( $log ) {				
		 
		  var service = {};
		  service.getCreditCardType = getCreditCardType;
		  return service;	  
	
		  function getCreditCardType( $creditCard ) {
			  var PATTERN_VISA = /^4[0-9]{6,}$/;
			  var PATTERN_MASTERCARD = /^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$/;
			  var PATTERN_AMERICAN_EXPRESS = /^3[47][0-9]{5,}$/;
			  var PATTERN_DINERS_CLUB = /^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/;			  

			  if( PATTERN_AMERICAN_EXPRESS.test($creditCard) ) {
				  return "AMEX";
			  }
			  
			  if( PATTERN_VISA.test($creditCard) ) {
				  return "VISA";
			  }

			  if( PATTERN_MASTERCARD.test($creditCard) ) {
				  return "MASTERCARD";
			  }			  
			  
			  if( PATTERN_DINERS_CLUB.test($creditCard) ) {
				  return "DINERSCLUB";
			  }			  
			  
			  return "UNKNOWN";
		  }
		  
		}

})();