(function () {
    'use strict';

    angular
        .module('app')
        .service( 'StorageService', StorageService );

    	StorageService.$inject = ['$log', '$window', '$localStorage' ];
    
		function StorageService( $log, $window, $localStorage ) {				
		 
		  var service = {};
		  service.getItem = getItem;
		  service.setItem = setItem;
		  service.clear = clear;
		  return service;
		
		  function getItem( $key ) {		
			  /*
			  if( $key in $window.localStorage ) {
				  	var data = $window.localStorage.getItem( $key );
              		$window.localStorage.removeItem( $key );
              		return( angular.extend( {}, angular.fromJson( data ) ) );
			  }
			  return null;
			  */
			  return angular.fromJson($localStorage[$key]);
		  }
		  
		  function setItem( $key, $value ) {
			  //$window.localStorage.setItem( $key, angular.toJson( $value ) );
			  $localStorage[$key] = angular.toJson($value);
		  }		  
		  
		  function clear() {
			 /* for( $key in $window.localStorage ) {
				  $window.localStorage.removeItem( $key );
			  }*/
			  $localStorage.$reset();
		  }
		  
		}

})();

