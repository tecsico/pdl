(function () {
    'use strict';
    angular.module('app')
            .value("trim", jQuery.trim)
            .factory("SearchCriteria", function ( ) {
                // Define the constructor function.
                function SearchCriteria( ) {
                    this.name = "";
                    this.surname = "";
                    this.propertyDetails = "";
                    this.status = "";
                    this.company = "";
                    this.startDate = null;
                    this.endDate = null;
                    this.user = null;
                    this.quoteNumber = "";
                    this.policyNumber = "";
                    this.dateOfBirth = null;
                    this.customerNumber = "";
                }
                return(SearchCriteria);
            })
            .factory("Title", function ( ) {
                // Define the constructor function.
                function Title( ) {
                    this.id = "";
                    this.description = "";
                    this.active = true;
                }

                return(Title);
            })
            .factory("Address", function ( ) {
                // Define the constructor function.
                function Address( ) {
                    this.number = "";
                    this.streetAddress = "";
                    this.postalCode = "";
                    this.state = "";
                    this.suburb = "";
                    this.addressPostalCode = null;
                    this.country = "";
                    this.streetType = "";
                    this.streetNumber = "";
                    this.propertyId = "";
                    this.unitNumber = "";
                    this.addressNotFound = false;
                }

                return(Address);
            })
            .factory("Lead", function (Address, Title) {
                // Define the constructor function.
                function Lead(name, surname) {

                    this.id = 0;
                    this.title = new Title();
                    this.name = name;
                    this.surname = surname;
                    this.mobile = "";
                    this.homeNumber = "";
                    this.workNumber = "";
                    this.source = "";
                    this.campaign = "";
                    this.email = "";
                    this.dateOfBirth = "";
                    this.status = null;
                    this.faxNumber = "";
                    this.landlordCompany = "";
                    this.landlordABN = "";
                    this.physicalAddress = new Address();
                    this.postalAddress = new Address();

                }

                return(Lead);
            })
            .factory("Customer", function (Address, Title) {
                // Define the constructor function.
                function Customer( ) {

                    this.id = 0;
                    this.title = new Title();
                    this.name = "";
                    this.surname = "";
                    this.mobile = "";
                    this.homeNumber = "";
                    this.workNumber = "";
                    this.source = "";
                    this.campaign = "";
                    this.email = "";
                    this.dateOfBirth = null;
                    //this.status = null;	                	
                    this.faxNumber = "";
                    this.customerCompanyName = "";
                    this.abn = "";
                    this.physicalAddress = new Address();
                    this.postalAddress = new Address();
                    this.customerReference = "";
                    this.company = "";
                }

                return(Customer);
            })
            .factory("CustomerRequest", function (Customer, User) {
                // Define the constructor function.
                function CustomerRequest( ) {

                    this.customer = undefined;
                    this.user = undefined;
                    this.customerId = "";

                }

                return(CustomerRequest);
            })
            .factory("User", function ( ) {
                // Define the constructor function.
                function User( ) {
                    this.id = 0;
                    this.username = "";
                    this.name = "";
                    this.surname = "";
                }

                return(User);
            })
            .factory("OwnerType", function ( ) {
                // Define the constructor function.
                function OwnerType( ) {
                    this.id = "";
                    this.description = "";
                    this.active = true;
                }

                return(OwnerType);
            })
            .factory("InsuranceType", function ( ) {
                // Define the constructor function.
                function InsuranceType( ) {
                    this.id = "";
                    this.description = "";
                    this.active = true;
                }

                return(InsuranceType);
            })
            .factory("PropertyType", function ( ) {
                // Define the constructor function.
                function PropertyType( ) {
                    this.id = "";
                    this.description = "";
                    this.active = true;
                }

                return(PropertyType);
            })
            .factory("Channel", function ( ) {
                // Define the constructor function.
                function Channel( ) {
                    this.id = "";
                    this.description = "";
                    this.active = true;
                }

                return(Channel);
            })
            .factory("QuotePolicyRule", function ( ) {
                // Define the constructor function.
                function QuotePolicyRule( ) {
                    this.id = 0;
                    this.ruleId = "";
                    this.value = "";
                }

                return(QuotePolicyRule);
            })
            .factory("Quote", function (OwnerType, InsuranceType, Channel) {
                // Define the constructor function.
                function Quote(  ) {
                    this.channel = new Channel();
                    this.ownerType = new OwnerType();
                    this.insuranceType = new InsuranceType();
                    this.product = {
                        id: undefined,
                        description: '',
                        produceQuote: false,
                        prePrice: false
                    };
                    this.policyRules = [];
                    this.status = undefined;
                    this.lastQuoteEmailed = undefined;
                    this.startDate = undefined;
                    this.endDate = undefined;
                    this.quoteRiskItems = [];
                }

                return(Quote);
            })
            .factory("QuoteRequest", function (Quote, User) {
                // Define the constructor function.
                function QuoteRequest(  ) {
                    this.quote = new Quote();
                    this.user = new User();
                    this.quoteCondition = null;
                }

                return(QuoteRequest);
            })
            .factory("QuoteNote", function (NoteType) {
                function QuoteNote(  ) {
                    this.quoteId = 0;
                    this.noteType = new NoteType();
                    this.note = '';
                }

                return(QuoteNote);
            })
            .factory("NoteType", function () {
                function NoteType(  ) {
                    this.process = '';
                    this.id = '';
                    this.description = '';
                }

                return(NoteType);
            })
            .factory("NavTrail", function () {
                function NavTrail(  ) {
                    this.trail = [];
                }

                return(NavTrail);
            })
            .factory("Policy", function () {
                function Policy(  ) {
                	
                }

                return(Policy);
            })            
            .factory("PolicyRequest", function (Quote, Policy, User) {
                // Define the constructor function.
                function PolicyRequest(  ) {
                    this.policy = new Policy();
                    this.policyCondition = null;
                    this.quote = new Quote();
                    this.user = new User();
                }

                return(PolicyRequest);
            })
            .factory("Payment", function () {
                // Define the constructor function.
                function Payment(  ) {

                }

                return(Payment);
            })
            .factory("PaymentRequest", function (Payment, Policy, User) {
                // Define the constructor function.
                function PaymentRequest(  ) {
                    this.policy = new Policy();
                    this.payment = new Payment();
                    this.user = new User();
                }

                return(PaymentRequest);
            })
            .factory("CommunicationRequest", function (User) {
                // Define the constructor function.
                function CommunicationRequest(  ) {
                    this.referenceId = null;
                    this.processType = null;
                    this.user = new User();
                    this.auditLogType = null;
                    this.brandId = null;
                    this.recipientType = null;
                }

                return(CommunicationRequest);
            })
            .factory("QuoteSpecifiedRiskItem", function () {
                // Define the constructor function.
                function QuoteSpecifiedRiskItem(  ) {
                    this.riskItemCode = undefined;
                    this.description = undefined;
                    this.value = undefined;
                    this.active = undefined;
                    this.increaseSumValue = true;
                }

                return(QuoteSpecifiedRiskItem);
            })
            .factory("QuoteRiskItem", function () {
                // Define the constructor function.
                function QuoteRiskItem(  ) {
                    this.riskCode = undefined;
                    this.specifiedRiskItems = [];
                }

                return(QuoteRiskItem);
            })
            .factory("Agent", function () {
                // Define the constructor function.
                function Agent(  ) {
                    this.agentId = '';
                    this.active = '';
                    this.name = '';
                    this.surname = '';
                    this.email = '';
                    this.mobile = '';
                    this.preferredMethodContact = {
                        id:'',
                        description:''
                    };
                }

                return(Agent);
            })
            .factory("ReportCriteria", function ( ) {
                // Define the constructor function.
                function ReportCriteria( ) {
                    this.statuses = [];
                    this.company = "";
                    this.startDate = null;
                    this.endDate = null;
                    this.month = null;
                    this.year = null;
                    this.branches = [];
                    this.format = null;
                    this.decision = null;
                    this.user = null;
                }
                return(ReportCriteria);
            })
            .factory("PolicyCondition", function () {
                // Define the constructor function.
                function PolicyCondition( ) {
                    this.createDate = null;
                    this.createdBy = null;
                    this.policyId = null;
                    this.statement = null;
                    this.active = null;
                    this.conditionId = null;
                }
                return(PolicyCondition);
            })
            .factory("QuoteCondition", function () {
                // Define the constructor function.
                function QuoteCondition( ) {
                    this.createDate = null;
                    this.createdBy = null;
                    this.quoteId = null;
                    this.statement = null;
                    this.active = null;
                    this.conditionId = null;
                }
                return(QuoteCondition);
            })
            .factory("Template", function () {
                // Define the constructor function.
                function Template() {
                    this.description = undefined;
                    this.brand = undefined;
                    this.subject = undefined;
                    this.content = undefined;
                    this.active = undefined;
                }
                return(Template);
            })
            .factory("EmailRecipient", function () {
                // Define the constructor function.
                function EmailRecipient() {
                    this.company = undefined;
                    this.name = undefined;
                    this.id = undefined;
                    this.email = undefined;
                }
                return(EmailRecipient);
            })
            .factory("CommunicationInstruction", function () {
                // Define the constructor function.
                function CommunicationInstruction(  ) {
                    this.email = null;
                    this.ccEmail = null;
                    this.destination = null;
                    this.templateId= null;
                    this.communicationSetup= null;
                    this.processType= null;
                    this.send = null;
                }

                return(CommunicationInstruction);
            });

})();