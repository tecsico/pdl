(function () {
    'use strict';

    angular
        .module('app')
        .service( 'QuoteService', QuoteService );

    	QuoteService.$inject = ['$http', '$log', '$rootScope'];
    
		function QuoteService( $http, $log, $rootScope ) {				
		 
		  var service = {};
		  service.quote = quote;
		  service.createQuote = createQuote;
		  service.createAccept = createAccept;
		  service.accept = accept;
		  service.acceptPayment = acceptPayment;
		  service.quickQuote = quickQuote;
		  service.getQuote = getQuote;		  
		  return service;
					  
		  function quote( $sessionId, $quote ) {
			  $quote.session = $sessionId;
			  $log.debug($quote);
		      var promise = $http.post( $rootScope.properties.services + 'interface/quote/json', $quote).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }
		  
		  function accept( $sessionId, $quote ) {
			  $quote.session = $sessionId;
			  $log.debug($quote);
		      var promise = $http.post( $rootScope.properties.services + 'interface/accept/json', $quote).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }		  

		  function acceptPayment( $sessionId, $quote ) {
			  $quote.session = $sessionId;
			  $log.debug($quote);
		      var promise = $http.post( $rootScope.properties.services + 'interface/acceptPayment/json', $quote).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }	
		  
		  function createQuote() {
			 return {
				 "client":{"name":"Steve Gell","registeredName":"ABC PTY LTD","brokerId":null,"postalAddress":{"address1":"POBox 3827","address2":null,"suburb":"Ringwoord","state":"VIC","postCode":"3136","country":"AUSTRALIA"},"streetAddress":{"address1":"417 Marrondah Highway","address2":"","suburb":"Ringwood","state":"VIC","postCode":"3136","country":"AUSTRALIA"},"telephone":"0398334251","fax":"","email":"steveg@gmail.com","fsraType":null,"contact":{"name":"Miss Susan Gell","telephone":"0397654321","mobile":"0446357364","fax":"","email":"susang@gmail.com"},"accountManager":{"name":"","telephone":"","mobile":"","fax":"","email":"","brokerId":null},"abn":"7377477772"},"code":"NewQuote","session":null,"policies":[{"policyNumber":"P98827","invoiceNumber":"INV8876","brokerId":null,"brokerPolicyId":"P98827","clazz":"LL","underwriter":{"name":"TEST Hollard Insurance Company Pty Ltd","brokerId":"HOLLAR0U","abn":"778374888"},"inceptionDate":"2015-12-15","expiryDate":"2016-12-15","insuredName":"Mr Steve Gell","previouslyFunded":"N","coInsurance":"N","fsraType":"F","secondaryReference":"","fundLoanNumber":null,"basePremium":485.0,"basePremiumGst":48.5,"stampDuty":53.35,"fsl":0.0,"brokerFee":275.0,"brokerFeeGst":27.5,"commission":0.0,"commissionGst":0.0,"premium":889.35,"numberOfPayments":10}],"funder":null,"ledgerId":null,"broker":{"abn":null,"username":"ferdi","password":"password","name":null,"telephone":null,"fax":null,"email":null,"contact":{"name":null,"telephone":null,"mobile":null,"fax":null,"email":null}},"software":{"company":"ABC Software PTY LTD","name":"Quotes","version":"1.0","authToken":"0d7547a1-46d9-4859-8b6f-f0b35ca3d030"},"bankingDetails":[]
				};
		  }
		  
		  function quickQuote( $sessionId, $quote ) {
			  $quote.session = $sessionId;
			  $log.debug($quote);
		      var promise = $http.post( $rootScope.properties.services + 'interface/quickquote/json', $quote).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }	
		  
		  function getQuote( $brokerId, $quoteId ) {
			  var $request = {
						"brokerId" : $brokerId,
					  	"externalReference" : $quoteId
					  };
			  $log.debug($request);
		      var promise = $http.post( $rootScope.properties.services + 'interface/retrieveQuote/json', $request).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }					  
		  
		  function createAccept($quoteNumber) {
			  $log.info('Creating accept request...');
			  return {
				"code" : "AcceptQuote",
				"quoteNumber" : $quoteNumber,
				"session" : "",
				"bankingDetails": 
					[
					]
			  };
		  }
		  
		}

})();