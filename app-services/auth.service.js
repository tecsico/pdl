(function () {
    'use strict';

    angular
        .module('app')
        .service( 'AuthService', AuthService );

    	AuthService.$inject = ['$http', '$log', '$cookies', '$rootScope', 'StorageService'];
    
		function AuthService( $http, $log, $cookies, $rootScope, StorageService ) {				
		 
		  var service = {};
		  service.authenticate = authenticate;	
		  service.signOut = signOut;
		  service.setCredentials = setCredentials;
		  service.isAuthenticated = isAuthenticated;
		  service.getCredentials = getCredentials;
		  service.hasPermission = hasPermission;
		  service.getBrokerByUsername = getBrokerByUsername;
		  return service;
		
		  function createLoginRequest( ) {
			  return {
					"broker": 
					{
						"abn": null,
						"username": null,
						"password": null,
						"name": null,
						"telephone": null,
						"fax": null,
						"email": null,
						"contact": 
						{
							"name": null,
							"telephone": null,
							"mobile": null,
							"fax": null,
							"email": null
						}
					},

					"software": 
					{
						"company": "ABC Software PTY LTD",
						"name": "Quotes",
						"version": "1.0",
						"authToken": "56b81bf5-060c-405c-a37d-2a193d6a04e9"
					}
				};
		  }
		  
		  function authenticate( $username, $password ) {	
			  var $request = createLoginRequest();
			  $request.broker.username = $username;
			  $request.broker.password = $password;
			  $log.info($request);
		      var promise = $http.post( $rootScope.properties.services + 'interface/authenticate/json', $request).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }
		  
		  function setCredentials( $username, $session) {	            

	            $rootScope.globals = {
	                "currentUser": {
	                	"username" : $username,
	                    "session": $session
	                }
	            };
	            
	            StorageService.setItem('currentUser',{
	                	"username" : $username,
	                    "session": $session
	            });
	            //$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
	            //$cookies.put('globals', $rootScope.globals);
	            //$log.info('setCredentials > ' + $rootScope.globals.currentUser.username);
	            //$log.info('setCredentials > ' + $rootScope.globals.currentUser.session);
	            
	            //$log.info('setCredentials > ' + getCredentials());
	            //$log.info('setCredentials > ' + getCredentials().username);
	            //$log.info('setCredentials > ' + getCredentials().session);
	        }		  
		  
		  function getCredentials() {
			  return StorageService.getItem('currentUser');
		  }
		  
		  function signOut() {
			  //$log.info('AuthService > signOut');			  
			  $cookies.remove('globals');
			  $rootScope.globals = {};
			  StorageService.clear();
		  }
		  
		  function isAuthenticated() {
			  //$log.info('AuthService > isAuthenticated ' + StorageService.getItem('currentUser') );
			  return StorageService.getItem( 'currentUser' );
		  }
		  
		  function hasPermission($permission) {
				if( $permission == 'admin' ) {
					if(this.getCredentials().username == 'ferdi' || this.getCredentials().username == 'johans' )
						return true;
				}
				return false;
		  }
		  
		  function getBrokerByUsername( $username ) {			  
		      var promise = $http.get( $rootScope.properties.services + 'admin/brokerByUsername/' + $username).success(function (data) {
		          return data;	          
		      });		   
		      return promise;
		  }
		  
		}

})();